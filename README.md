# Angular Class Management App

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 12.1.2.

## Prerequisites

Follow this [link](https://angular.io/guide/setup-local) to make sure your machine is correctly setup.

## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Project Requirements

Create a class management application to showcase your basic Angular skills. A class should have a name, start date, and a list of students. Each student should have a name and age. You can design the objects in whichever way you see fit.

### Expectations

- A page with a list of all the classes. This page should show each class' name and start date.
- Upon clicking a class, the user should be redirected to new page. The new page should show the list of students for that class sorted by name.
- Once done, share a link to your github or bitbucket code.

## Questions or Clarification

Please feel free to reach out if you have any clarifying questions regarding this assignment.
